import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'pump-catalog',
    pathMatch: 'full',
  },
  {
    path: 'pump-catalog',
    loadChildren: () =>
      import('./pump-catalog/pump-catalog.module').then(
        (m) => m.PumpCatalogPageModule
      ),
  },
  {
    path: 'project-name',
    loadChildren: () => import('./project-name/project-name.module').then( m => m.ProjectNamePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
