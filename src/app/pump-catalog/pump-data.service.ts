import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IOrderModel } from '../project-name/order.model';

@Injectable({
  providedIn: 'root',
})
export class PumpDataService {
  storedPumpData: any;
  private _storedPumpQuantity = 1;

  constructor(private http: HttpClient) {}

  getPumpData() {
    return this.http.get(`https://localhost:44368/api/pumps`);
  }

  getSinglePump(id: string) {
    return this.http.get(`https://localhost:44368/api/pumps/${id}`);
  }

  storePumpData(data: any) {
    this.storedPumpData = data;
  }

  get savedPumpQuantity() {
    return this._storedPumpQuantity;
  }

  get retrieveStoredPumpData() {
    return this.storedPumpData;
  }

  updateStoredPumpQuantity(quantity: number) {
    this._storedPumpQuantity = quantity;
    console.log('changing stored pump quantity', this._storedPumpQuantity);
  }

  postPumpOrder(ProjectName: string, AppID: any, Quantity: number) {
    const dataToPost = new IOrderModel(ProjectName, AppID, Quantity);
    return this.http.post<any>(`https://localhost:44368/api/projects`, {
      ...dataToPost,
    });
  }
}
