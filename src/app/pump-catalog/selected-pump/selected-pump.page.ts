import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { PumpDataService } from '../pump-data.service';

@Component({
  selector: 'app-selected-pump',
  templateUrl: './selected-pump.page.html',
  styleUrls: ['./selected-pump.page.scss'],
})
export class SelectedPumpPage implements OnInit {
  pumpQuantity: any;
  isLoading = false;
  loadedPumpRecord: any;
  constructor(
    private pumpDataService: PumpDataService,
    private activatedRoute: ActivatedRoute,
    private alertCrtl: AlertController,
    private router: Router
  ) {}

  stylePumpStockClass(stock: number) {
    if (stock > 0 && stock <= 3) {
      return 'less-stock';
    }
    if (stock > 3) {
      return 'plenty-stock';
    }
    if (stock == 0) {
      return 'no-stock';
    }
  }

  incrementPumpQuantity() {
    this.pumpQuantity++;
    this.pumpDataService.updateStoredPumpQuantity(this.pumpQuantity);
  }

  decrementPumpQuantity() {
    if (this.pumpQuantity == 1) {
      return;
    }
    this.pumpQuantity--;
    this.pumpDataService.updateStoredPumpQuantity(this.pumpQuantity);
  }

  ngOnInit() {
    this.isLoading = true;
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('pumpId')) {
        return;
      }
      const clickedPumpId = paramMap.get('pumpId');
      this.pumpDataService.getSinglePump(clickedPumpId).subscribe((data) => {
        this.pumpDataService.storePumpData(data);
        this.loadedPumpRecord = data;
        this.pumpQuantity = this.pumpDataService.savedPumpQuantity;
        console.log('single pump data', this.loadedPumpRecord);
        this.isLoading = false;
      });
    });
  }

  discard() {
    this.alertCrtl
      .create({
        header: 'Confirm?',
        message: 'Do you want to discard this Pump Order?',
        buttons: [
          { text: 'Cancel', role: 'cancel' },
          {
            text: 'Discard',
            handler: () => {
              this.router.navigateByUrl('/pump-catalog');
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }
}
