import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectedPumpPage } from './selected-pump.page';

const routes: Routes = [
  {
    path: '',
    component: SelectedPumpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectedPumpPageRoutingModule {}
