import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectedPumpPageRoutingModule } from './selected-pump-routing.module';

import { SelectedPumpPage } from './selected-pump.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectedPumpPageRoutingModule
  ],
  declarations: [SelectedPumpPage]
})
export class SelectedPumpPageModule {}
