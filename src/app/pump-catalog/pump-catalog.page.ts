import { Component, OnInit } from '@angular/core';
import { PumpDataService } from './pump-data.service';

@Component({
  selector: 'app-pump-catalog',
  templateUrl: './pump-catalog.page.html',
  styleUrls: ['./pump-catalog.page.scss'],
})
export class PumpCatalogPage implements OnInit {
  isLoading = false;
  fetchedPumpRecords: any;
  constructor(private pumpDataService: PumpDataService) {}

  ngOnInit() {}

  stylePumpStockClass(stock: number) {
    if (stock > 0 && stock <= 3) {
      return 'less-stock';
    }
    if (stock > 3) {
      return 'plenty-stock';
    }
    if (stock == 0) {
      return 'no-stock';
    }
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.pumpDataService.updateStoredPumpQuantity(1);
    this.pumpDataService.getPumpData().subscribe((data) => {
      this.fetchedPumpRecords = data;
      console.log('data from .net', this.fetchedPumpRecords);
      this.isLoading = false;
    });
  }
}
