import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PumpCatalogPage } from './pump-catalog.page';

const routes: Routes = [
  {
    path: '',
    component: PumpCatalogPage,
  },
  {
    path: ':pumpId',
    loadChildren: () =>
      import('./selected-pump/selected-pump.module').then(
        (m) => m.SelectedPumpPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PumpCatalogPageRoutingModule {}
