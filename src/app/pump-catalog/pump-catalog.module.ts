import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PumpCatalogPageRoutingModule } from './pump-catalog-routing.module';

import { PumpCatalogPage } from './pump-catalog.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PumpCatalogPageRoutingModule
  ],
  declarations: [PumpCatalogPage]
})
export class PumpCatalogPageModule {}
