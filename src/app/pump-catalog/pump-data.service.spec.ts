import { TestBed } from '@angular/core/testing';

import { PumpDataService } from './pump-data.service';

describe('PumpDataService', () => {
  let service: PumpDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PumpDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
