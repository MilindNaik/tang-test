import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { PumpDataService } from '../pump-catalog/pump-data.service';

@Component({
  selector: 'app-project-name',
  templateUrl: './project-name.page.html',
  styleUrls: ['./project-name.page.scss'],
})
export class ProjectNamePage implements OnInit {
  isLoading = false;
  loadedPumpData: any;
  loadedPumpQuantity: number;
  loadedPumpId: string;

  constructor(
    private pumpDataService: PumpDataService,
    private router: Router,
    private alertCrtl: AlertController,
    private loadingController: LoadingController
  ) {}

  stylePumpStockClass(stock: number) {
    if (stock > 0 && stock <= 3) {
      return 'less-stock';
    }
    if (stock > 3) {
      return 'plenty-stock';
    }
    if (stock == 0) {
      return 'no-stock';
    }
  }

  goBack() {
    this.alertCrtl
      .create({
        header: 'Confirm?',
        message: 'Do you want to Edit this Pump Order?',
        buttons: [
          { text: 'Cancel', role: 'cancel' },
          {
            text: 'Yes',
            handler: () => {
              this.router.navigateByUrl(`/pump-catalog/${this.loadedPumpId}`);
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }

  ngOnInit() {
    this.isLoading = true;
    this.loadedPumpData = this.pumpDataService.retrieveStoredPumpData;
    this.loadedPumpQuantity = this.pumpDataService.savedPumpQuantity;
    this.loadedPumpId = this.loadedPumpData.Pumps.AppID;
    console.log(
      'Project Page pump data',
      this.loadedPumpData,
      this.loadedPumpQuantity,
      this.loadedPumpId
    );
    this.isLoading = false;
  }

  onSubmit(form: NgForm) {
    this.loadingController
      .create({
        keyboardClose: true,
        message: 'Submitting Order Details',
      })
      .then((loadingEl) => {
        loadingEl.present();
        this.pumpDataService
          .postPumpOrder(
            form.form.value.ProjectName,
            this.loadedPumpId,
            this.loadedPumpQuantity
          )
          .subscribe(() => {
            console.log('pump order added');
          });
        loadingEl.dismiss();
        form.reset();
      });
    this.alertCrtl
      .create({
        header: 'Success',
        message: 'Order Placed',
        buttons: [
          {
            text: 'Okay',
            handler: () => {
              this.router.navigateByUrl('/pump-catalog');
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }
}
