import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProjectNamePageRoutingModule } from './project-name-routing.module';

import { ProjectNamePage } from './project-name.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProjectNamePageRoutingModule
  ],
  declarations: [ProjectNamePage]
})
export class ProjectNamePageModule {}
