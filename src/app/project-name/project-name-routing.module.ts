import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectNamePage } from './project-name.page';

const routes: Routes = [
  {
    path: '',
    component: ProjectNamePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectNamePageRoutingModule {}
